// https://josephluck.co.uk/blog/next-typescript-monorepo


const path = require("path");
// Add to the array below for any sibling packages
const withTranspileModules = require('next-transpile-modules')([]);

let nextConfig = {
  webpack5: false
};

module.exports = withTranspileModules(nextConfig)
