import { globalFontFace } from "@vanilla-extract/css"

globalFontFace('anagraphRegular', {
    src: `url('/assets/fonts/Regular.woff2') format('woff2'), url('/assets/fonts/Regular.woff') format('woff')`,
    fontDisplay: 'block',
    fontWeight: 'normal',
    fontStyle: 'normal'
})

globalFontFace('anagraphLight', {
    src: `url('/assets/fonts/Light.woff2') format('woff2'), url('/assets/fonts/Light.woff') format('woff')`,
    fontDisplay: 'block',
    fontWeight: 300,
    fontStyle: 'normal'
})
