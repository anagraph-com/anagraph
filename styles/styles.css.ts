import { globalStyle } from "@vanilla-extract/css"
import { createSprinkles, defineProperties } from "@vanilla-extract/sprinkles"

const colours = {
    white: "#FFFFFF",
    black: "#111111"
}

const fonts = {
    "regular": "anagraphRegular",
    "light": "anagraphLight"
}

const fontSizes = {
    sm: "0.75vw",
    md: "1vw",
    lg: "2vw",
    xl: "3vw",
    xxl: "4.5vw",
    massive: "8vw"
}

const lineHeights = {
    body: "1.6em",
    headline: "1.2em"
}

const properties = defineProperties({
    properties: {
        color: colours,
        background: colours,
        fontFamily: fonts,
        fontSize: fontSizes,
        lineHeight: lineHeights
    }
})

globalStyle("a:hover", {
    transform: "translateY(2px)"
})

globalStyle("svg *", {
    vectorEffect: "non-scaling-stroke"
})

globalStyle("html", {
    backgroundColor: "black"
})

const sprinkles = createSprinkles(properties)
export default sprinkles
