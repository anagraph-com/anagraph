import { globalStyle } from "@vanilla-extract/css"
import { createSprinkles, defineProperties } from "@vanilla-extract/sprinkles"

const properties = defineProperties({
    defaultCondition: "mobile",
    conditions: {
        mobile: {},
        tablet: { '@media': 'screen and (min-width: 768px)' },
        desktop: { '@media': 'screen and (min-width: 1024px)' }
    },
    properties: {
        border: ["1px solid red"],
        boxSizing: ["border-box"],
        display: ["flex", "grid"],
        position: ["relative", "fixed", "absolute", "sticky"],
        gridColumn: {
            fill: "1 / -1",
            inset: "2 / -2",
            leftAnchor: "2 / span 7",
            centre: "10 / span 8"
        },
        gridRow: {
            main: "2 / -2",
            topMargin: "1 / 1",
            bottomMargin: "3 / span 1"
        },
        gridTemplateColumns: {
            master: "4rem repeat(24, 1fr) 4rem",
            masterNarrow: "3rem repeat(24, 1fr) 3rem"
        },
        gridTemplateRows: {
            fitContainer: "4rem auto 4rem",
            fitContent: "1fr auto 1fr"
        },
        justifyContent: ["center"],
        alignItems: ["center"],
        color: {
            white: "#FFFFFF",
            black: "#111111",
            currentColor: "currentColor"
        },
        backgroundColor: {
            white: "#FFFFFF",
            black: "#111111",
            currentColor: "currentColor"
        },
        fontFamily: {
            "regular": "anagraphRegular",
            "light": "anagraphLight"
        },
        fontSize: {
            "0.5": "0.5rem",
            "0.75": "0.75rem",
            "1": "1rem",
            "1.5": "1.5rem",
            "2": "2rem",
            "3": "3rem",
            "4.5": "4.5rem",
            "8": "8rem"
        },
        textAlign: ["center"],
        lineHeight: {
            body: "1.6em",
            headline: "1.2em"
        },
        height: {
            sm: "2rem",
            md: "4rem",
            md2: "8rem",
            full: "100%",
            screen: "100vh"
        },
        marginTop: {
            neg: "-4rem"
        },
        marginBottom: {
            neg: "-4rem"
        },
        top: ["4rem", "0"],
        width: ["1px"],
        left: ["50%"],
        opacity: ["0.5"],
        flexDirection: ["row", "column"],
        overflowX: ["hidden"],
        listStyle: ["none"],
        rowGap: ["0.25em", "1rem", "2rem", "4rem"]
    }
})

export const sprinkles = createSprinkles(properties)

export type Sprinkles = Parameters<typeof sprinkles>[0]

globalStyle("a:hover", {
    transform: "translateY(2px)"
})

globalStyle("svg *", {
    vectorEffect: "non-scaling-stroke"
})

globalStyle("html", {
    backgroundColor: "black"
})

// TEXT

export const TextBase = sprinkles({
    fontFamily: "light"
})

export const TextSm = sprinkles({
    fontSize: "0.75",
    fontFamily: "light",
    color: "white",
    lineHeight: "body"
})

export const TextMd = sprinkles({
    fontSize: "1",
    fontFamily: "light",
    color: "white",
    lineHeight: "body"
})

export const TextLg = sprinkles({
    fontSize: {
        mobile: "1",
        tablet: "2"
    },
    fontFamily: "light",
    color: "white",
    lineHeight: "headline"
})

export const TextXl = sprinkles({
    fontSize: {
        mobile: "2",
        tablet: "3"
    },
    fontFamily: "light",
    color: "white",
    lineHeight: "headline"
})

export const TextXxl = sprinkles({
    fontSize: {
        mobile: "3",
        tablet: "4.5"
    },
    fontFamily: "light",
    color: "white",
    lineHeight: "headline"
})

export const TopicsList = sprinkles({
    fontSize: {
        mobile: "2",
        tablet: "4.5"
    },
    fontFamily: "light",
    color: "white",
    lineHeight: "headline",
    listStyle: "none",
    display: "flex",
    flexDirection: "column",
    rowGap: "1rem"
})

export const TextMassive = sprinkles({
    fontSize: {
        mobile: "3",
        tablet: "8"
    },
    fontFamily: "light",
    color: "white",
    lineHeight: "headline"
})

// LAYOUT

export const GridH = sprinkles({
    display: "grid",
    flexDirection: "column",
    gridColumn: "fill",
    gridTemplateColumns: {
        mobile: "masterNarrow",
        tablet: "master"
    }
})

export const GridV = sprinkles({
    display: "grid",
    flexDirection: "column",
    gridTemplateRows: "fitContainer"
})

export const GridVGap = sprinkles({
    rowGap: {
        mobile: "2rem",
        tablet: "4rem"
    }
})

export const GridAnchorTop = sprinkles({
    gridRow: "main"
})

export const GridAnchorLeft = sprinkles({
    gridColumn: {
        mobile: "inset",
        tablet: "leftAnchor"
    }
})

export const GridHBody = sprinkles({
    gridColumn: "inset"
})

export const GridVBody = sprinkles({
    gridRow: "main"
})

export const GridHCentre = sprinkles({
    gridColumn: {
        mobile: "inset",
        desktop: "centre"
    }
})

export const CentreContents = sprinkles({
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
})

export const ScreenHeight = sprinkles({
    height: "screen"
})

export const Sticky = sprinkles({
    position: {
        mobile: "relative",
        tablet: "sticky"
    },
    top: {
        mobile: "0",
        tablet: "4rem"
    }
})

export const FitContent = sprinkles({
    gridTemplateRows: "fitContent"
})

// COMPONENTS

export const SpineTop = sprinkles({
    gridColumn: "fill",
    position: "relative",
    gridRow: "topMargin"
})

export const SpineBottom = sprinkles({
    gridColumn: "fill",
    position: "relative",
    gridRow: "bottomMargin"
})

export const SpineBody = sprinkles({
    gridColumn: "fill",
    position: "relative",
    gridRow: "main"
})

export const AbsoluteSpine = sprinkles({
    position: "absolute",
    width: "1px",
    height: "full",
    left: "50%",
    top: "0",
    backgroundColor: "currentColor",
    opacity: "0.5"
})

export const StoryBeat = sprinkles({
    fontFamily: "light",
    boxSizing: "border-box",
    fontSize: {
        mobile: "1.5",
        tablet: "1.5",
        desktop: "2"
    },
})

export const TextCentre = sprinkles({
    textAlign: "center"
})
