import { FC, PropsWithChildren } from "react";

export const Section: FC<PropsWithChildren & { noMinHeight?: boolean }> = ({ children, noMinHeight }) => <section style={{
        position: "relative",
        boxSizing: "border-box",
        display: "grid",
        gridTemplateColumns: "6vw repeat(24,1fr) 6vw",
        gridAutoRows: "minmax(min-content, max-content)",
        gridColumnGap: "1rem",
        minHeight: noMinHeight ? "auto" : "100vh"
}}>{children}</section>