import { FC } from "react";
import { LogoStyle } from "../ui/Layout.css";
import { TextBase } from "../ui/Text.css";
import { AnagraphIcon } from "./AnagraphIcon";

export const AnagraphLogo: FC = () => <div className={LogoStyle}>
  <h1 className={`${TextBase}`}><div style={{ display: "inline-block", position: "relative", top: "0.05em", marginRight: "0.25em" }}><AnagraphIcon /></div>Anagraph</h1>
</div>