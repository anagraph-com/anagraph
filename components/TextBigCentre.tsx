import { FC, PropsWithChildren } from "react";
import { TextLg } from "../ui/Text.css";

export const TextBigCentre: FC<PropsWithChildren> = ({ children }) => <div style={{
    gridColumn: "9 / span 10",
    gridRow: "1 / -1",
    marginTop: "4rem",
    marginBottom: "4rem"
}}>
    <div className={`${TextLg}`} style={{
        textAlign: "center"
    }}>{children}</div>
</div>