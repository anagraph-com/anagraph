import { Property } from "csstype";
import { FC } from "react";

export const Spine: FC<{ height?: Property.Height, fillHeight?: boolean, short?: boolean }> = ({ height, fillHeight, short }) => <div style={{
    position: "absolute",
    left: "50%",
    width: "1px",
    height: height,
    backgroundColor: "currentColor",
    opacity: 0.25
}} />