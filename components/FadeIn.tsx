import { FC, PropsWithChildren, useEffect, useRef, useState } from "react";
import { useWindowScrollPosition } from "rooks";
import { useWindowSize } from "usehooks-ts";

const FadeIn: FC<PropsWithChildren & { duration?: number, minimumOpacity?: number, yThreshold?: number, slideIn?: boolean }> = ({ children, duration = 0.1, minimumOpacity = 0, yThreshold = 0.8, slideIn }) => {
    const { scrollY } = useWindowScrollPosition()
    const { height: windowHeight } = useWindowSize()
    const ref = useRef<HTMLDivElement>(null)
    const rect = useRef<DOMRect | null>(null)
    const [yPos, setYPos] = useState(0)

    useEffect(() => {
        setYPos(Math.abs(rect.current ? ((rect.current.top + rect.current.height / 2) / windowHeight) : 0))
    }, [windowHeight, rect.current])

    useEffect(() => {
        rect.current = ref?.current?.getBoundingClientRect() ?? null
    }, [ref, scrollY])
    
    return <div
        ref={ref}
        style={{
            transitionProperty: "opacity transform",
            transitionDuration: `${duration}s`,
            transform: slideIn ? `translateX(${yPos < yThreshold ? 0 : 0.25}em)` : 'none',
            opacity: yPos < yThreshold ? 1 : minimumOpacity
        }}>
        {children}
    </div>
}

export default FadeIn