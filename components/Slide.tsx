import { Children, FC, PropsWithChildren, useEffect, useRef, useState } from "react";
import { useWindowScrollPosition } from "rooks";
import { useWindowSize } from "usehooks-ts";
import { TextXxl } from "../ui/Text.css";

const Slide: FC<PropsWithChildren & { duration?: number, minimumOpacity?: number, yThreshold?: number }> = ({ children }) => {
    const { scrollY } = useWindowScrollPosition()
    const { height: windowHeight, width: windowWidth } = useWindowSize()
    const slider = useRef<HTMLDivElement>(null)
    const container = useRef<HTMLDivElement>(null)
    const anchor = useRef<HTMLDivElement>(null)
    const sliderRect = useRef<DOMRect | null>(null)
    const containerRect = useRef<DOMRect | null>(null)
    const anchorRect = useRef<DOMRect | null>(null)
    const [xPos, setXPos] = useState(0)
    const [numChildren, setNumChildren] = useState<number>(0)

    const [activeIndex, setActiveIndex] = useState<number>(-1)

    useEffect(() => {
        if (!containerRect?.current || !sliderRect?.current || !anchorRect?.current) {
            setXPos(windowWidth)
            return
        }

        const percent = anchorRect.current.y / (containerRect.current.height) * -1

        setActiveIndex(Math.round(percent))

        setXPos(-percent * containerRect.current.width)
    }, [windowHeight, sliderRect.current, containerRect.current, anchorRect.current])

    useEffect(() => {
        sliderRect.current = slider?.current?.getBoundingClientRect() ?? null
    }, [slider, scrollY])

    useEffect(() => {
        containerRect.current = container?.current?.getBoundingClientRect() ?? null
    }, [container, scrollY])

    useEffect(() => {
        anchorRect.current = anchor?.current?.getBoundingClientRect() ?? null
    }, [anchor, scrollY])

    useEffect(() => {
        setNumChildren(Children.count(children))
    }, [children])

    return <>
        <div ref={anchor} style={{ position: "absolute", top: 0 }} />
        <div
            style={{
                position: "fixed",
                display: "grid",
                gridTemplateColumns: "6vw repeat(24,1fr) 6vw",
                gridAutoFlow: "column",
                top: 0,
                left: 0,
                height: "100%",
                width: "100%",
                overflowX: "hidden",
                boxSizing: "border-box"
            }}>
            <div
                ref={container}
                style={{
                    position: "relative",
                    gridColumn: "8 / span 12"
                }}>
                <div
                    ref={slider}
                    style={{
                        position: "relative",
                        alignItems: "center",
                        top: "50%",

                        transform: `translateY(-50%) translateX(${xPos}px)`,
                        width: "100%",
                        height: "100%",
                        boxSizing: "border-box",
                        display: "flex",
                    }}>
                    <div ref={container} style={{
                        position: "absolute",
                        width: `${numChildren * 100}%`,
                        display: "grid",
                        gridTemplateColumns: `repeat(${numChildren}, 1fr)`,
                        gridAutoFlow: "column"
                    }}>
                        {
                            Children.map(children, (child, i) => {
                                return <div style={{ position: "relative" }}>
                                    <h3
                                        className={`${TextXxl}`}
                                        style={{
                                            position: "relative",
                                            width: "100%",
                                            textAlign: "center",
                                            opacity: activeIndex > -1 && activeIndex < numChildren ? (activeIndex == i ? 1 : 0.25) : 0,
                                            transitionProperty: "opacity transform",
                                            transitionDuration: `0.5s`,
                                        }}>
                                        {child}
                                    </h3>
                                    {i < numChildren - 1 ? <div style={{ width: 0 }}>
                                        <svg style={{
                                            position: "absolute",
                                            right: 0,
                                            top: "35%",
                                            height: "50%",
                                            transform: "translateX(50%)",
                                            opacity: 0.25

                                        }} viewBox="0 0 14 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1 1L13 13L1 25" stroke="currentColor" />
                                        </svg>
                                    </div>
                                        : null}
                                </div>
                            })
                        }
                    </div>
                </div>
            </div>
        </div>
    </>
}

export default Slide