import { FC, PropsWithChildren, useEffect, useRef, useState } from "react";
import { useWindowSize } from "usehooks-ts";

const FadeInHorizontal: FC<PropsWithChildren & { duration?: number, minimumOpacity?: number, yThreshold?: number }> = ({ children, duration = 0.1, minimumOpacity = 0, yThreshold = 0.666 }) => {
    // const { scrollY } = useWindowScrollPosition()
    const { height: windowHeight, width: windowWidth } = useWindowSize()
    const ref = useRef<HTMLDivElement>(null)
    const rect = useRef<DOMRect | null>(null)
    const [opacity, setOpacity] = useState(0)

    useEffect(() => {
        if(!rect.current) return
        const x = ((rect.current.x + rect.current.width / 2) / windowWidth ?? 0)
        // setOpacity(1 - Math.abs((x - 0.5) * 2))
        const compressed = (1 - Math.abs((x - 0.5) * 2))
        setOpacity((compressed > 0.5) ? 1 : 0.25)
    }, [windowHeight, rect.current])

    useEffect(() => {
        rect.current = ref?.current?.getBoundingClientRect() ?? null
    }, [ref, scrollY])

    return <div
        ref={ref}
        style={{
            transitionProperty: "opacity transform",
            transitionDuration: `${duration}s`,
            // transform: `translateX(${yPos < yThreshold ? 0 : 0.25}em)`,
            opacity: opacity,
            // transitionTimingFunction: "cubic-bezier(0.75, 0, 0, 0.75)",
            transitionDelay: "0s"
        }}>
        {children}
    </div>
}

export default FadeInHorizontal