import { FC, PropsWithChildren } from "react"
import { TextCaps, TextSm } from "../ui/Text.css"
import FadeIn from "./FadeIn"


export const SectionTitle: FC<PropsWithChildren> = ({ children }) => <div style={{
    gridColumn: "1 / span 13",
    gridRow: "1",
    minHeight: "5vw",
    left: 0,
    top: "5vw",
    position: "sticky"
}}>
    <FadeIn duration={0.5}>
        <div style={{
            display: "block",
            left: 0,
            width: "fit-content",
            borderTop: "1px solid currentColor",
            paddingLeft: "2rem",
            paddingTop: "1em"
        }}>
            <h2 className={`${TextSm} ${TextCaps}`}>{children}</h2>
        </div>
    </FadeIn>
</div>