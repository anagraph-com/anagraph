import { style } from "@vanilla-extract/css"

const ScreenHeightStyle = style({
    minHeight: "100vh"
})

const VerticalCentreStyle = style({
    display: "flex",
    flexDirection: "column",
    justifyContent: "center"
})

const ParallaxScreenHeightStyle = style({
    minHeight: "300vh"
})

// const HeroStyle = style({
//     display: "grid",
//     gridTemplateColumns: "repeat(24,1fr)",
//     gridTemplateRows: "repeat(24,1fr)",
//     gridColumnGap: "1.5rem",
//     columnGap: "1.5rem",
//     gridAutoRows: "auto",
//     boxSizing: "border-box",
//     padding: "5vw",
//     height: "100vh"
// })

const MenuStyle = style({
    gridColumn: "-4 / -1",
    textAlign: "right"
})

const TaglineStyle = style({
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-end",
    gridColumn: "1 / 12",
    gridRow: "12 / -1"
})

const SectionHeadlineStyle = style({
    gridColumn: "2 / span 8",
    position: "sticky",
    top: "5vw"
})

const SectionHeroStyle = style({
    gridColumn: "2 / span 9",
    gridRow: "1 / 1",
    position: "sticky",
    top: "5vw",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "left",
    gap: "4em",
    marginBottom: "5vw",
    // paddingTop: "3vw",
    height: "fit-content",
    minHeight: "20rem"
})

const SectionBodyStyle = style({
    gridColumn: "15 / span 11",
    // gridRow: "2"
})

const LogoLayoutStyle = style({
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translateX(-50%) translateY(-50%)"
})

const LogoStyle = style({
    gridColumn: "1 / span 12",
    textTransform: "lowercase"
})

const OfferingStyle = style({
    display: "grid",
    gridColumn: "1 / 24",
    gridTemplateColumns: "repeat(24, 1fr)",
    height: "100vh",
    boxSizing: "border-box"
})

const OfferingIconWrapperStyle = style({
    position: "relative",
    boxSizing: "border-box",
    display: "grid",
    gridTemplateColumns: "6vw repeat(24,1fr) 6vw",
    gridTemplateRows: "auto 1fr",
    gridColumnGap: "1rem"
})

const OfferingIconStyle = style({
    position: "relative",
    gridColumn: "10 / span 8",
    top: 0
})

const OfferingTextContainerStyle = style({
    gridColumnStart: 18,
    gridColumnEnd: 22,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    gap: "2rem"
})

export {
    SectionHeroStyle,
    // HeroStyle,
    MenuStyle,
    TaglineStyle,
    SectionHeadlineStyle,
    SectionBodyStyle,
    LogoStyle,
    LogoLayoutStyle,
    OfferingStyle,
    OfferingIconWrapperStyle,
    OfferingIconStyle,
    OfferingTextContainerStyle,
    ScreenHeightStyle,
    ParallaxScreenHeightStyle,
    VerticalCentreStyle
}
