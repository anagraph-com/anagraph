const Chat = () => <svg viewBox="0 0 128 128" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g opacity="0.5">
        <path d="M55.473 85.5H2.02703C1.18367 85.5 0.5 86.8431 0.5 88.5V113.5C0.5 115.157 1.18367 116.5 2.02703 116.5H4V122L9.5 116.5H55.473C56.3163 116.5 57 115.157 57 113.5V88.5C57 86.8431 56.3163 85.5 55.473 85.5Z" stroke="currentColor" strokeDasharray="4 4" />
    </g>
    <circle cx="18.5" cy="100.5" r="2.5" stroke="currentColor" />
    <circle cx="28.5" cy="100.5" r="2.5" stroke="currentColor" />
    <circle cx="38.5" cy="100.5" r="2.5" stroke="currentColor" />
    <path d="M110 63C110 62.1636 110.564 62 111 62C111.436 62 112 62.2727 112 63C112 64.0909 111 63.9636 111 64.8C111 64.8291 111 65.0061 111 65.0909" stroke="currentColor" />
    <path d="M108.5 5.5H3.5C1.84315 5.5 0.5 6.84315 0.5 8.5V33.5C0.5 35.1569 1.84315 36.5 3.5 36.5H4V42.5L10 36.5H108.5C110.157 36.5 111.5 35.1569 111.5 33.5V8.5C111.5 6.84315 110.157 5.5 108.5 5.5Z" stroke="currentColor" />
    <path d="M19.5 45.5H124.5C126.157 45.5 127.5 46.8431 127.5 48.5V73.5C127.5 75.1569 126.157 76.5 124.5 76.5H124V82.5L118 76.5H19.5C17.8431 76.5 16.5 75.1569 16.5 73.5V48.5C16.5 46.8431 17.8431 45.5 19.5 45.5Z" stroke="currentColor" />
    <g opacity="0.5">
        <path d="M101.5 16H8.5" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M91.5 26H8.5" stroke="currentColor" strokeDasharray="4 4" />
    </g>
    <g opacity="0.5">
        <path d="M117.5 56H24.5" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M107.5 66H24.5" stroke="currentColor" strokeDasharray="4 4" />
    </g>
    <path d="M111.25 66H110.75" stroke="currentColor" />
</svg>

export default Chat