const Next = () => <svg viewBox="0 0 128 128" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path opacity="0.5" d="M79 16H0" stroke="currentColor" strokeDasharray="4 4" />
    <path d="M79 16L127 64L79 112" stroke="currentColor" />
    <path d="M63 16L111 64L63 112" stroke="currentColor" />
    <path opacity="0.5" d="M79 112H0" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M127 64H48" stroke="currentColor" strokeDasharray="4 4" />
</svg>

export default Next