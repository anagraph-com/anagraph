const Signpost = () => <svg viewBox="0 0 128 128" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M64 16V128" stroke="currentColor" />
    <path d="M64 36H10L2 44L10 52H64" stroke="currentColor" />
    <path opacity="0.5" d="M7 44H16" stroke="currentColor" strokeDasharray="4 4" />
    <path d="M64 76H10L2 84L10 92H64" stroke="currentColor" />
    <path d="M64 16H118L126 24L118 32H64" stroke="currentColor" />
    <path d="M64 56H118L126 64L118 72H64" stroke="currentColor" />
    <path opacity="0.5" d="M7 44L11 40" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M7 44L11 48" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M7 84H16" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M7 84L11 80" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M7 84L11 88" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M121 24H112" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M121 24L117 20" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M121 24L117 28" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M121 64H112" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M121 64L117 60" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M121 64L117 68" stroke="currentColor" strokeDasharray="4 4" />
</svg>

export default Signpost