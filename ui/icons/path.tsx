const Path = () => <svg viewBox="0 0 128 128" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M16 16C9.37258 16 4 21.3726 4 28H28C28 21.3726 22.6274 16 16 16Z" stroke="currentColor" />
    <circle cx="16" cy="7" r="6" stroke="currentColor" />
    <path d="M68 28L76 36" stroke="currentColor" />
    <path d="M76 28L68 36" stroke="currentColor" />
    <path d="M100 28L108 36" stroke="currentColor" />
    <path d="M108 28L100 36" stroke="currentColor" />
    <path d="M12 76L20 84" stroke="currentColor" />
    <path d="M20 76L12 84" stroke="currentColor" />
    <path d="M36 108L44 116" stroke="currentColor" />
    <path d="M44 108L36 116" stroke="currentColor" />
    <path d="M68 108L76 116" stroke="currentColor" />
    <path d="M76 108L68 116" stroke="currentColor" />
    <path d="M68 76L76 84" stroke="currentColor" />
    <path d="M76 76L68 84" stroke="currentColor" />
    <path d="M36 44L44 52" stroke="currentColor" />
    <path d="M44 44L36 52" stroke="currentColor" />
    <path opacity="0.5" d="M112 111C112 119 104 127 96 127C88 127 80 127 72 127C64 127 56 119 56 111C56 103 48 96 40 96C32 96 25 96 17 96C9 96 1 88 1 80C1 72 9 64 17 64C25 64 96 64 104 64C112 64 120 56 120 48C120 40 120 40 120 32C120 24 112 16 104 16C96 16 88 24 88 32C88 40 80 48 72 48C64 48 56 40 56 32C56 24 48 16 40 16H32" stroke="currentColor" strokeDasharray="4 4" />
    <path d="M116.5 93L110.071 100L107.5 97.2" stroke="currentColor" />
    <circle cx="112" cy="96" r="12" stroke="currentColor" />
</svg>

export default Path