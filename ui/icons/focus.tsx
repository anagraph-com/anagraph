const Focus = () => <svg viewBox="0 0 128 128" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M13.5 127C6.03164 110.21 1.5 88.1551 1.5 64C1.5 39.8449 6.03164 17.79 13.5 1" stroke="currentColor" />
    <path d="M13.5 127C20.9684 110.21 25.5 88.1551 25.5 64C25.5 39.8449 20.9684 17.79 13.5 1" stroke="currentColor" />
    <g opacity="0.5">
        <path d="M95 64L13.5 127" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M95 64L13.5 1" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M127 64H1" stroke="currentColor" strokeDasharray="4 4" />
    </g>
</svg>

export default Focus