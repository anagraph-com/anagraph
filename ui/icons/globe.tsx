const Globe = () => <svg viewBox="0 0 128 128" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g clipPath="url(#clip0)">
        <circle opacity="0.5" cx="64" cy="64" r="62.2243" transform="rotate(-60 64 64)" stroke="currentColor" strokeDasharray="4 4" />
        <ellipse opacity="0.5" cx="64" cy="64.0001" rx="62.2243" ry="7.77804" transform="rotate(-60 64 64.0001)" stroke="currentColor" strokeDasharray="4 4" />
        <ellipse opacity="0.5" cx="64" cy="64" rx="62.2243" ry="23.3341" transform="rotate(-60 64 64)" stroke="currentColor" strokeDasharray="4 4" />
        <ellipse opacity="0.5" cx="64" cy="64.0001" rx="62.2243" ry="38.8902" transform="rotate(-60 64 64.0001)" stroke="currentColor" strokeDasharray="4 4" />
        <ellipse opacity="0.5" cx="64" cy="64.0001" rx="62.2243" ry="54.4463" transform="rotate(-60 64 64.0001)" stroke="currentColor" strokeDasharray="4 4" />
        <ellipse opacity="0.5" cx="64" cy="64.0001" rx="62.2243" ry="7.77804" transform="rotate(-120 64 64.0001)" stroke="currentColor" strokeDasharray="4 4" />
        <ellipse opacity="0.5" cx="64" cy="64.0001" rx="62.2243" ry="23.3341" transform="rotate(-120 64 64.0001)" stroke="currentColor" strokeDasharray="4 4" />
        <ellipse opacity="0.5" cx="64" cy="64.0001" rx="62.2243" ry="38.8902" transform="rotate(-120 64 64.0001)" stroke="currentColor" strokeDasharray="4 4" />
        <ellipse opacity="0.5" cx="64" cy="64.0001" rx="62.2243" ry="54.4463" transform="rotate(-120 64 64.0001)" stroke="currentColor" strokeDasharray="4 4" />
        <circle cx="20.5" cy="34.5" r="1.5" stroke="currentColor" />
        <circle cx="75.5" cy="85.5" r="1.5" stroke="currentColor" />
        <circle cx="75.5" cy="120.5" r="1.5" stroke="currentColor" />
        <circle cx="92.5" cy="91.5" r="1.5" stroke="currentColor" />
        <circle cx="110.5" cy="78.5" r="1.5" stroke="currentColor" />
        <circle cx="62.5" cy="25.5" r="1.5" stroke="currentColor" />
        <circle cx="59.5" cy="6.5" r="1.5" stroke="currentColor" />
        <circle cx="47.5" cy="107.5" r="1.5" stroke="currentColor" />
        <circle cx="72.5" cy="29.5" r="1.5" stroke="currentColor" />
        <circle cx="105.5" cy="30.5" r="1.5" stroke="currentColor" />
        <circle cx="56.5" cy="31.5" r="1.5" stroke="currentColor" />
        <circle cx="14.5" cy="80.5" r="1.5" stroke="currentColor" />
    </g>
    <defs>
        <clipPath id="clip0">
            <rect width="128" height="128" fill="white" />
        </clipPath>
    </defs>
</svg>

export default Globe