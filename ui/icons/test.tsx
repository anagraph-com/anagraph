const Test = () => <svg viewBox="0 0 128 128" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M44.5 71.5C44.5 70.3954 45.3954 69.5 46.5 69.5H122.5C123.605 69.5 124.5 70.3954 124.5 71.5V124.5H44.5V71.5Z" stroke="currentColor" />
    <path d="M41.5 124.5H127.5V124.5C127.5 126.157 126.157 127.5 124.5 127.5H44.5C42.8431 127.5 41.5 126.157 41.5 124.5V124.5Z" stroke="currentColor" />
    <rect x="0.5" y="45" width="12" height="13" rx="2" stroke="currentColor" />
    <path d="M9.5 58L9.5 61C9.5 61.5523 9.05228 62 8.5 62L4.5 62C3.94772 62 3.5 61.5523 3.5 61L3.5 58" stroke="currentColor" />
    <path d="M9.5 45L9.5 42C9.5 41.4477 9.05228 41 8.5 41L4.5 41C3.94772 41 3.5 41.4477 3.5 42L3.5 45" stroke="currentColor" />
    <rect x="77.5" y="0.5" width="44" height="14" rx="2" transform="rotate(90 77.5 0.5)" stroke="currentColor" />
    <circle cx="73" cy="10.5" r="1.5" transform="rotate(90 73 10.5)" stroke="currentColor" />
    <circle cx="68" cy="10.5" r="1.5" transform="rotate(90 68 10.5)" stroke="currentColor" />
    <circle cx="68" cy="15.5" r="1.5" transform="rotate(90 68 15.5)" stroke="currentColor" />
    <circle cx="68" cy="20.5" r="1.5" transform="rotate(90 68 20.5)" stroke="currentColor" />
    <rect x="74.5" y="14" width="8" height="3" rx="1.5" transform="rotate(90 74.5 14)" stroke="currentColor" />
    <rect x="0.5" y="76.5" width="35" height="51" rx="2" stroke="currentColor" />
    <rect x="39.5" y="0.5" width="21" height="39" rx="2" transform="rotate(90 39.5 0.5)" stroke="currentColor" />
    <circle cx="106.5" cy="54.5" r="1.39062" transform="rotate(90 106.5 54.5)" stroke="currentColor" strokeWidth="1.21875" />
    <rect x="124.5" y="0.5" width="59" height="37" rx="2" transform="rotate(90 124.5 0.5)" stroke="currentColor" />
    <path opacity="0.5" d="M112 25L103 34L99 30" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M92 92L83 101L79 97" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M43 37L34 46L30 42" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M27 6L18 15L14 11" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M24 97L15 106L11 102" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M9 50L6 53L4.5 51.5" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M73 30L70 33L68.5 31.5" stroke="currentColor" strokeDasharray="4 4" />
    <path d="M47.5317 27.1322L25.7192 33.8437C25.3463 37.3859 24.6006 45.8127 24.6006 51.1819C24.6006 57.8934 25.7192 62.3678 38.0237 62.3678C50.3282 62.3678 51.4468 56.2155 50.8875 51.1819C50.44 47.155 48.4639 33.4709 47.5317 27.1322Z" stroke="currentColor" />
    <path d="M24.6006 51.7412H50.8875" stroke="currentColor" />
</svg>

export default Test