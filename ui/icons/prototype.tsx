const Prototype = () => <svg viewBox="0 0 128 128" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M64 40L88 64L64 88L40 64L64 40Z" stroke="currentColor" />
    <path d="M32 38V32H38" stroke="currentColor" />
    <path d="M0.5 0.5V8.5H8.5V0.5H0.5Z" stroke="currentColor" />
    <path d="M119.5 0.5V8.5H127.5V0.5H119.5Z" stroke="currentColor" />
    <path d="M0.5 119.5V127.5H8.5V119.5H0.5Z" stroke="currentColor" />
    <path d="M119.5 119.5V127.5H127.5V119.5H119.5Z" stroke="currentColor" />
    <path d="M96 38V32H90" stroke="currentColor" />
    <path d="M32 90V96H38" stroke="currentColor" />
    <path d="M96 90V96H90" stroke="currentColor" />
    <path d="M0 32H6" stroke="currentColor" />
    <path d="M0 40H6" stroke="currentColor" />
    <path d="M0 56H6" stroke="currentColor" />
    <path d="M0 72H6" stroke="currentColor" />
    <path d="M0 88H6" stroke="currentColor" />
    <path d="M0 96H6" stroke="currentColor" />
    <path d="M0 80H6" stroke="currentColor" />
    <path d="M0 64H6" stroke="currentColor" />
    <path d="M0 48H6" stroke="currentColor" />
    <path d="M0 36H3" stroke="currentColor" />
    <path d="M0 52H3" stroke="currentColor" />
    <path d="M0 68H3" stroke="currentColor" />
    <path d="M0 84H3" stroke="currentColor" />
    <path d="M0 92H3" stroke="currentColor" />
    <path d="M0 76H3" stroke="currentColor" />
    <path d="M0 60H3" stroke="currentColor" />
    <path d="M0 44H3" stroke="currentColor" />
    <path d="M128 32H122" stroke="currentColor" />
    <path d="M128 40H122" stroke="currentColor" />
    <path d="M128 56H122" stroke="currentColor" />
    <path d="M128 72H122" stroke="currentColor" />
    <path d="M128 88H122" stroke="currentColor" />
    <path d="M128 96H122" stroke="currentColor" />
    <path d="M128 80H122" stroke="currentColor" />
    <path d="M128 64H122" stroke="currentColor" />
    <path d="M128 48H122" stroke="currentColor" />
    <path d="M128 36H125" stroke="currentColor" />
    <path d="M128 52H125" stroke="currentColor" />
    <path d="M128 68H125" stroke="currentColor" />
    <path d="M128 84H125" stroke="currentColor" />
    <path d="M128 92H125" stroke="currentColor" />
    <path d="M128 76H125" stroke="currentColor" />
    <path d="M128 60H125" stroke="currentColor" />
    <path d="M128 44H125" stroke="currentColor" />
    <path d="M96 0L96 6" stroke="currentColor" />
    <path d="M88 0L88 6" stroke="currentColor" />
    <path d="M72 0L72 6" stroke="currentColor" />
    <path d="M56 0L56 6" stroke="currentColor" />
    <path d="M40 0L40 6" stroke="currentColor" />
    <path d="M32 0L32 6" stroke="currentColor" />
    <path d="M48 0L48 6" stroke="currentColor" />
    <path d="M64 0L64 6" stroke="currentColor" />
    <path d="M80 0L80 6" stroke="currentColor" />
    <path d="M92 0L92 3" stroke="currentColor" />
    <path d="M76 0L76 3" stroke="currentColor" />
    <path d="M60 0L60 3" stroke="currentColor" />
    <path d="M44 0L44 3" stroke="currentColor" />
    <path d="M36 0L36 3" stroke="currentColor" />
    <path d="M52 0L52 3" stroke="currentColor" />
    <path d="M68 0L68 3" stroke="currentColor" />
    <path d="M84 0L84 3" stroke="currentColor" />
    <path d="M96 128L96 122" stroke="currentColor" />
    <path d="M88 128L88 122" stroke="currentColor" />
    <path d="M72 128L72 122" stroke="currentColor" />
    <path d="M56 128L56 122" stroke="currentColor" />
    <path d="M40 128L40 122" stroke="currentColor" />
    <path d="M32 128L32 122" stroke="currentColor" />
    <path d="M48 128L48 122" stroke="currentColor" />
    <path d="M64 128L64 122" stroke="currentColor" />
    <path d="M80 128L80 122" stroke="currentColor" />
    <path d="M92 128L92 125" stroke="currentColor" />
    <path d="M76 128L76 125" stroke="currentColor" />
    <path d="M60 128L60 125" stroke="currentColor" />
    <path d="M44 128L44 125" stroke="currentColor" />
    <path d="M36 128L36 125" stroke="currentColor" />
    <path d="M52 128L52 125" stroke="currentColor" />
    <path d="M68 128L68 125" stroke="currentColor" />
    <path d="M84 128L84 125" stroke="currentColor" />
    <g opacity="0.5">
        <path d="M64 64V88" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M64 64H88" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M64 64H40" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M64 64V40" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M32 32L8.5 8.5" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M96 32L119.5 8.5" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M32 96L8.5 119.5" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M96 96L119.5 119.5" stroke="currentColor" strokeDasharray="4 4" />
    </g>
</svg>

export default Prototype