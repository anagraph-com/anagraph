const Chart = () => <svg viewBox="0 0 128 128" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g opacity="0.5">
        <path d="M2 67V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M18 32V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M34 96V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M50 64V68" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M66 64V80" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M82 48V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M98 64V31" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M114 90V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M10 53V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M26 64V48" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M42 80V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M58 32V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M74 48V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M90 32V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M106 64V60" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M122 81V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M6 60V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M22 16V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M38 64V72" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M54 48V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M70 64V72" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M86 35V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M102 64V48" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M118 64V76" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M14 47V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M30 92V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M46 64V73" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M62 97V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M78 36V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M94 0V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M110 69V64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M126 64V69" stroke="currentColor" strokeDasharray="4 4" />
    </g>
    <path d="M4 66V64" stroke="currentColor" />
    <path d="M20 96V64" stroke="currentColor" />
    <path d="M36 112V64" stroke="currentColor" />
    <path d="M52 72V64" stroke="currentColor" />
    <path d="M68 84V64" stroke="currentColor" />
    <path d="M84 69V64" stroke="currentColor" />
    <path d="M100 86V64" stroke="currentColor" />
    <path d="M116 80V64" stroke="currentColor" />
    <path d="M12 72V64" stroke="currentColor" />
    <path d="M28 80V64" stroke="currentColor" />
    <path d="M44 75V64" stroke="currentColor" />
    <path d="M60 112V64" stroke="currentColor" />
    <path d="M76 96V64" stroke="currentColor" />
    <path d="M92 81V64" stroke="currentColor" />
    <path d="M108 80V64" stroke="currentColor" />
    <path d="M124 96V64" stroke="currentColor" />
    <path d="M8 69V64" stroke="currentColor" />
    <path d="M24 72V64" stroke="currentColor" />
    <path d="M40 88V64" stroke="currentColor" />
    <path d="M56 80V64" stroke="currentColor" />
    <path d="M72 77V64" stroke="currentColor" />
    <path d="M88 76V64" stroke="currentColor" />
    <path d="M104 73V64" stroke="currentColor" />
    <path d="M120 89V64" stroke="currentColor" />
    <path d="M16 80V64" stroke="currentColor" />
    <path d="M32 96V64" stroke="currentColor" />
    <path d="M48 68V64" stroke="currentColor" />
    <path d="M64 104V64" stroke="currentColor" />
    <path d="M80 81V64" stroke="currentColor" />
    <path d="M96 112V64" stroke="currentColor" />
    <path d="M112 73V64" stroke="currentColor" />
</svg>

export default Chart