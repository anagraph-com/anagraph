const Sprint = () => <svg viewBox="0 0 128 128" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M64 16C37.4903 16 16 37.4903 16 64C16 86.9029 32.0405 106.059 53.5 110.848" stroke="currentColor"/>
<path d="M64 112C90.5097 112 112 90.5097 112 64C112 37.4903 90.5097 16 64 16" stroke="currentColor"/>
<path d="M76 112H128" stroke="currentColor" strokeDasharray="4 4"/>
<path d="M0 112H64" stroke="currentColor"/>
</svg>


export default Sprint