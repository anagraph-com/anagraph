const Analyse = () => <svg viewBox="0 0 128 128" fill="none" xmlns="http://www.w3.org/2000/svg">
    <circle cx="64" cy="64" r="8" stroke="currentColor" />
    <circle cx="8.5" cy="64" r="8" stroke="currentColor" />
    <circle cx="8.5" cy="8.5" r="8" stroke="currentColor" />
    <circle cx="64" cy="8.5" r="8" stroke="currentColor" />
    <circle cx="119.5" cy="8.5" r="8" stroke="currentColor" />
    <circle cx="119.5" cy="64" r="8" stroke="currentColor" />
    <circle cx="119.5" cy="119.5" r="8" stroke="currentColor" />
    <circle cx="64" cy="119.5" r="8" stroke="currentColor" />
    <circle cx="8.5" cy="119.5" r="8" stroke="currentColor" />
    <mask id="hideLines" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="128" height="128">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M128 0H0V128H128V0ZM64 72C68.4183 72 72 68.4183 72 64C72 59.5817 68.4183 56 64 56C59.5817 56 56 59.5817 56 64C56 68.4183 59.5817 72 64 72ZM16.5 64C16.5 68.4183 12.9183 72 8.5 72C4.08172 72 0.5 68.4183 0.5 64C0.5 59.5817 4.08172 56 8.5 56C12.9183 56 16.5 59.5817 16.5 64ZM8.5 16.5C12.9183 16.5 16.5 12.9183 16.5 8.5C16.5 4.08172 12.9183 0.5 8.5 0.5C4.08172 0.5 0.5 4.08172 0.5 8.5C0.5 12.9183 4.08172 16.5 8.5 16.5ZM72 8.5C72 12.9183 68.4183 16.5 64 16.5C59.5817 16.5 56 12.9183 56 8.5C56 4.08172 59.5817 0.5 64 0.5C68.4183 0.5 72 4.08172 72 8.5ZM119.5 16.5C123.918 16.5 127.5 12.9183 127.5 8.5C127.5 4.08172 123.918 0.5 119.5 0.5C115.082 0.5 111.5 4.08172 111.5 8.5C111.5 12.9183 115.082 16.5 119.5 16.5ZM127.5 64C127.5 68.4183 123.918 72 119.5 72C115.082 72 111.5 68.4183 111.5 64C111.5 59.5817 115.082 56 119.5 56C123.918 56 127.5 59.5817 127.5 64ZM119.5 127.5C123.918 127.5 127.5 123.918 127.5 119.5C127.5 115.082 123.918 111.5 119.5 111.5C115.082 111.5 111.5 115.082 111.5 119.5C111.5 123.918 115.082 127.5 119.5 127.5ZM72 119.5C72 123.918 68.4183 127.5 64 127.5C59.5817 127.5 56 123.918 56 119.5C56 115.082 59.5817 111.5 64 111.5C68.4183 111.5 72 115.082 72 119.5ZM8.5 127.5C12.9183 127.5 16.5 123.918 16.5 119.5C16.5 115.082 12.9183 111.5 8.5 111.5C4.08172 111.5 0.5 115.082 0.5 119.5C0.5 123.918 4.08172 127.5 8.5 127.5Z" fill="currentColor" />
    </mask>
    <g mask="url(#hideLines)">
        <g opacity="0.5">
            <path d="M120 120C104.536 120 92 107.464 92 92C92 76.536 104.536 64 120 64" stroke="currentColor" strokeDasharray="4 4" />
            <path d="M8 64C8 48.536 20.536 36 36 36C51.464 36 64 48.536 64 64" stroke="currentColor" strokeDasharray="4 4" />
            <path d="M64 8C79.464 8 92 20.536 92 36C92 51.464 79.464 64 64 64" stroke="currentColor" strokeDasharray="4 4" />
            <path d="M8 120C8 89.072 33.0721 64 64 64" stroke="currentColor" strokeDasharray="4 4" />
            <path d="M120 120C89.0721 120 64 94.9279 64 64" stroke="currentColor" strokeDasharray="4 4" />
            <path d="M64 120C33.0721 120 8 94.9279 8 64" stroke="currentColor" strokeDasharray="4 4" />
            <path d="M64 64C33.0721 64 8 38.9279 8 8" stroke="currentColor" strokeDasharray="4 4" />
            <path d="M120 8C120 38.9279 94.9279 64 64 64" stroke="currentColor" strokeDasharray="4 4" />
        </g>
    </g>
</svg>

export default Analyse