const Funnel = () => <svg viewBox="0 0 128 129" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M60 118.809C60.4085 120.851 61.634 121.668 63.8809 121.668C66.1277 121.668 67.5574 120.136 67.5574 118.809C67.5574 116.051 65.2085 115.847 63.7787 115.438" stroke="currentColor" />
    <path d="M67.149 112.621C66.8426 111.251 65.6916 110.23 63.6877 110.23C61.9404 110.23 60.4085 111.251 60.4085 112.621C60.4085 114.926 62.5035 115.097 63.7787 115.438" stroke="currentColor" />
    <path d="M63.6766 108.8V123.2" stroke="currentColor" />
    <circle cx="64" cy="116" r="12" stroke="currentColor" />
    <path d="M6.5 8C3.18629 8 0.5 10.6863 0.5 14H12.5C12.5 10.6863 9.81371 8 6.5 8Z" stroke="currentColor" />
    <circle cx="6.5" cy="3.5" r="3" stroke="currentColor" />
    <path d="M121.5 8C118.186 8 115.5 10.6863 115.5 14H127.5C127.5 10.6863 124.814 8 121.5 8Z" stroke="currentColor" />
    <circle cx="121.5" cy="3.5" r="3" stroke="currentColor" />
    <path d="M64 8C60.6863 8 58 10.6863 58 14H70C70 10.6863 67.3137 8 64 8Z" stroke="currentColor" />
    <circle cx="64" cy="3.5" r="3" stroke="currentColor" />
    <path d="M35 8C31.6863 8 29 10.6863 29 14H41C41 10.6863 38.3137 8 35 8Z" stroke="currentColor" />
    <circle cx="35" cy="3.5" r="3" stroke="currentColor" />
    <path d="M93 8C89.6863 8 87 10.6863 87 14H99C99 10.6863 96.3137 8 93 8Z" stroke="currentColor" />
    <circle cx="93" cy="3.5" r="3" stroke="currentColor" />
    <path opacity="0.5" d="M64 80L6 19" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M64 80L122 19" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M64 80L35 19" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M64 80L93 19" stroke="currentColor" strokeDasharray="4 4" />
    <path opacity="0.5" d="M64 96V19" stroke="currentColor" strokeDasharray="4 4" />
</svg>

export default Funnel