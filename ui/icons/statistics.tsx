const Statistics = () => <svg viewBox="0 0 130 130" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path opacity="0.25" d="M1 113H17L33 89L65 81L81 57L113 33L129 9" stroke="currentColor" strokeDasharray="2 2" />
    <path d="M129 0.5L113 10L105 25L81 41L41 105L1 113" stroke="currentColor" />
    <rect x="5" y="121" width="28" height="8" stroke="currentColor" />
    <rect x="37" y="113" width="28" height="16" stroke="currentColor" />
    <rect x="69" y="97" width="28" height="32" stroke="currentColor" />
    <rect x="101" y="49" width="28" height="80" stroke="currentColor" />
</svg>

export default Statistics