const Plan = () => <svg viewBox="0 0 128 128" fill="none" xmlns="http://www.w3.org/2000/svg">
    <rect x="0.5" y="8" width="40" height="24" stroke="currentColor" />
    <rect x="4.5" y="12" width="8" height="16" stroke="currentColor" />
    <rect x="16.5" y="12" width="8" height="16" stroke="currentColor" />
    <rect x="28.5" y="12" width="8" height="16" stroke="currentColor" />
    <rect x="87.5" y="96" width="40" height="24" stroke="currentColor" />
    <rect x="91.5" y="100" width="32" height="4" stroke="currentColor" />
    <rect x="91.5" y="107" width="8" height="9" stroke="currentColor" />
    <rect x="102.5" y="107" width="21" height="9" stroke="currentColor" />
    <rect x="48" y="113" width="11" height="3" stroke="currentColor" />
    <rect x="48" y="100" width="2" height="2" stroke="currentColor" />
    <rect x="52" y="100" width="7" height="2" stroke="currentColor" />
    <rect x="48" y="104" width="2" height="2" stroke="currentColor" />
    <rect x="52" y="104" width="7" height="2" stroke="currentColor" />
    <rect x="48" y="108" width="2" height="2" stroke="currentColor" />
    <rect x="52" y="108" width="7" height="2" stroke="currentColor" />
    <rect x="44" y="96" width="40" height="24" stroke="currentColor" />
    <rect x="64" y="100" width="16" height="10" stroke="currentColor" />
    <rect x="87.5" y="52" width="40" height="24" stroke="currentColor" />
    <rect x="90.5" y="60" width="21" height="9" stroke="currentColor" />
    <rect x="114.5" y="60" width="11" height="9" stroke="currentColor" />
    <rect x="0.5" y="52" width="40" height="24" stroke="currentColor" />
    <rect x="4.5" y="56" width="32" height="16" stroke="currentColor" />
    <path d="M25.5 56L26.5 58L28.5 62V65L23.5 69.5L11.5 59.5L4.5 62" stroke="currentColor" />
    <path d="M13 72L12 69V67L14 69L17.5 70L20 72" stroke="currentColor" />
    <path d="M31 72L33.5 70.5V66.5L35 62.5L36.5 61.5" stroke="currentColor" />
    <path d="M22.0606 61.4393C22.6464 62.0251 22.6464 62.9749 22.0606 63.5607C21.7677 63.8536 21 64.5 21 64.5C21 64.5 20.2322 63.8536 19.9393 63.5607C19.3535 62.9749 19.3535 62.0251 19.9393 61.4393C20.5251 60.8536 21.4749 60.8536 22.0606 61.4393Z" stroke="currentColor" />
    <rect x="44" y="8" width="40" height="24" stroke="currentColor" />
    <rect x="48" y="13" width="14" height="2" stroke="currentColor" />
    <rect x="66" y="13" width="14" height="2" stroke="currentColor" />
    <rect x="48" y="19" width="14" height="2" stroke="currentColor" />
    <rect x="66" y="19" width="14" height="2" stroke="currentColor" />
    <rect x="48" y="25" width="14" height="2" stroke="currentColor" />
    <rect x="66" y="25" width="14" height="2" stroke="currentColor" />
    <rect x="87.5" y="8" width="40" height="24" stroke="currentColor" />
    <circle cx="99.5" cy="20" r="8" stroke="currentColor" />
    <circle cx="115.5" cy="20" r="8" stroke="currentColor" />
    <rect x="0.5" y="96" width="40" height="24" stroke="currentColor" />
    <rect x="14.5" y="107" width="12" height="9" stroke="currentColor" />
    <path d="M16.5 104C16.5 101.791 18.2909 100 20.5 100V100C22.7091 100 24.5 101.791 24.5 104V107H16.5V104Z" stroke="currentColor" />
    <g opacity="0.5">
        <path d="M64 32C64 49.6731 53.2548 64 40 64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M64 96C64 78.3269 74.7452 64 88 64" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M20 32V52" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M108 76V96" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M88 20H84" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M44 108H40" stroke="currentColor" strokeDasharray="2 2" />
        <path d="M108 32V52" stroke="currentColor" strokeDasharray="4 4" />
        <path d="M96 32L32 96" stroke="currentColor" strokeDasharray="4 4" />
    </g>
</svg>

export default Plan