import { style } from "@vanilla-extract/css"
import sprinkles from "../styles/styles.css"

export const TextBase = sprinkles({
    // fontSize: "xxl",
    fontFamily: "light",
    // color: "white",
    // lineHeight: "headline"
})

export const TextCaps = style({
    textTransform: "uppercase",
    letterSpacing: "0.1em"    
})

export const TextSm = sprinkles({
    fontSize: "sm",
    fontFamily: "light",
    color: "white",
    lineHeight: "body"
})

export const TextMd = sprinkles({
    fontSize: "md",
    fontFamily: "light",
    color: "white",
    lineHeight: "body"
})

export const TextLg = sprinkles({
    fontSize: "lg",
    fontFamily: "light",
    color: "white",
    lineHeight: "headline"
})

export const TextXl = sprinkles({
    fontSize: "xl",
    fontFamily: "light",
    color: "white",
    lineHeight: "headline"
})

export const TextXxl = sprinkles({
    fontSize: "xxl",
    fontFamily: "light",
    color: "white",
    lineHeight: "headline"
})

export const TextCentre = style({
    textAlign: "center"
})

export const TextMassive = sprinkles({
    fontSize: "massive",
    fontFamily: "light",
    color: "white",
    lineHeight: "headline"
})

export const TextMarked = style({
    position: "relative",
    paddingTop: "0.75em",
    selectors: {
        "&:before": {
            display: "block",
            position: "absolute",
            top: 0,
            content: "",
            width: "0.55em",
            height: "0.04em",
            backgroundColor: "currentColor"
        }
    }
})

export const BigList = style({
    listStyle: "none",
    lineHeight: "1.5em"
})

export const LinkButton = style({
    cursor: "pointer"
})