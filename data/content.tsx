import { FC } from 'react'
import Chat from '../ui/icons/chat'
import Globe from '../ui/icons/globe'
import OpenSource from '../ui/icons/open-source'
import Path from '../ui/icons/path'
import Plan from '../ui/icons/plan'
import Prototype from '../ui/icons/prototype'
import Sprint from '../ui/icons/sprint'
import Test from '../ui/icons/test'

const PROCESS: { label: string, body: FC, icon: FC }[] = [
    {
      label: "Workshop",
      body: () => "Understanding and prioritising your project scope is a crucial phase of developing a successful product. We will help you to clarify your goals and specify the scope of work.",
      icon: () => <Chat />
    },
    {
      label: "Design",
      body: () => "User Experience (UX) and User Interface (UI) design are about making your product useful and usable. We design user interfaces to be simple and enjoyable to use.",
      icon: () => <Plan />
    },
    {
      label: "Plan",
      body: () => "We plan out and estimate every feature in the greatest detail possible, providing you with the opportunity to evaluate and prioritise features so that you can get the best ROI on time spent.",
      icon: () => <Path />
    },
    {
      label: "Prototype",
      body: () => "Rapid prototyping enables us to discover limitations and opportunities early on in the project, ensuring that the final product is the best it can be.",
      icon: () => <Prototype />
    },
    {
      label: "Test",
      body: () => "We consult users for feedback and perform various automated tests to ensure products are robust and usable in real-world conditions.",
      icon: () => <Test />
    },
    {
      label: "Deploy",
      body: () => "We deploy instances of your product into the world, monitoring usage and collecting feedback on performance, stability and user experience.",
      icon: () => <Globe />
    },
    {
      label: "Iterate",
      body: () => "No product is ever perfected on the first iteration. After soft-launching protoypes into the field, we continually improve products based on user feedback, optimising performance and usability.",
      icon: () => <Sprint />
    },
    {
      label: "Scale",
      body: () => "When the prototype is stable, we will get your product production-ready – setting up automated build processes that enable you to scale production capacity to meet the market.",
      icon: () => <OpenSource />
    }
  ]
  
  const COPY_TEXT: string[] = [
    "Anagraph is an Industry 4.0 consultancy with a hybrid skillset spanning UX design, research and software development.",
    "We specialise in designing and building monitoring systems, realtime video and control interfaces for industrial environments.",
    "Our team brings a unique combination of creative and technical disciplines.",
    "We work with clients to refine concepts, design UX prototypes and build solutions in a single continuous process.",
    "For more information on how we can help you on your next project, contact us to line up a video chat or in-person meeting."
  ]
  
  const TOPICS = [
    "IIoT",
    "Automation",
    "Data Acquisition",
    "Video Streaming",
    "UX Design",
    "UI Design",
    "Computer Vision",
    "Vehicle Telemetry",
    "Geospatial Data",
    "Machine Learning",
    "Remote Control",
    "Analytics"
  ]

  export {
  COPY_TEXT, PROCESS, TOPICS
}
