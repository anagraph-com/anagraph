import { FC } from "react";
import { AnagraphLogo } from "../components/AnagraphLogo";
import { COPY_TEXT, PROCESS, TOPICS } from "../data/content";
import { AbsoluteSpine, CentreContents, FitContent, GridAnchorLeft, GridAnchorTop, GridH, GridHBody, GridHCentre, GridV, GridVBody, GridVGap, ScreenHeight, SpineBody, SpineBottom, SpineTop, Sticky, StoryBeat, TextCentre, TextLg, TextXxl, TopicsList, sprinkles } from "../styles/new/stylesNew.css";
import Globe from "../ui/icons/globe";
import RoundTable from "../ui/icons/round-table";

const Spine = {
  Top: () => <div className={`${SpineTop}`}>
    <div className={AbsoluteSpine} />
  </div>,
  Bottom: () => <div className={`${SpineBottom}`}>
    <div className={AbsoluteSpine} />
  </div>,
  Body: () => <div className={`${SpineBody}`}>
    <div className={AbsoluteSpine} />
  </div>
}

const IndexNew: FC = () => {
  return <div className={sprinkles({
    backgroundColor: "black",
    color: "white"
  })}>
    <div className={`${GridH} ${GridV} ${ScreenHeight}`}>
      <div className={`${GridHBody} ${GridVBody} ${CentreContents}`}>
        <div className={TextXxl}><AnagraphLogo /></div>
      </div>
      <Spine.Bottom />
    </div>
    <div className={`${GridH} ${GridV} ${GridVGap} ${FitContent} ${ScreenHeight}`}>
      <Spine.Top />
      <div className={`${GridVBody} ${GridHCentre} ${CentreContents}`}>
        <p className={`${TextLg} ${TextCentre} ${StoryBeat}`}><br />{COPY_TEXT[0]}<br /><br /></p>
      </div>
      <div className={`${GridVBody} ${GridHCentre}`} style={{ opacity: 0.5 }}>
        <RoundTable />
      </div>
      <Spine.Bottom />
    </div>
    <div className={`${GridH} ${GridV} ${GridVGap} ${ScreenHeight} ${FitContent}`}>
      <Spine.Top />
      <div className={`${GridVBody} ${GridHCentre}`}>
        <Globe />
      </div>
      <div className={`${GridVBody} ${GridHCentre} ${CentreContents}`}>
        <p className={`${TextLg} ${TextCentre} ${StoryBeat}`}><br />{COPY_TEXT[1]}<br /><br /></p>
      </div>
      <Spine.Bottom />
    </div>
    <div className={`${GridH} ${GridV}`} style={{ position: "relative" }}>
      <Spine.Top />
      <Spine.Body />
      <div className={`${GridAnchorLeft} ${GridAnchorTop}`}>
        <div className={Sticky}>
          <p className={`${TextLg} ${StoryBeat}`}>{COPY_TEXT[2]}</p>
        </div>
      </div>
      <div className={`${GridHBody} ${GridVBody}`} style={{
        position: "relative",
        gridColumn: "14 / span 10"
      }}>
        <ul className={TopicsList}>
          {
            TOPICS.map((topic, i) =>
              <li
                key={i}
                style={{
                  paddingLeft: "0.5em",
                  position: "relative"
                }}>
                <div style={{
                  display: "block",
                  position: "absolute",
                  left: 0,
                  top: "0.5em",
                  width: "0.2em",
                  height: "0.2em",
                  border: "0.5px solid currentColor",
                  transform: "translateX(-50%) translateX(0.5px)",
                  borderRadius: "50%",
                  backgroundColor: "#111111"
                }} />
                {topic}</li>
            )
          }
        </ul>
      </div>
      <Spine.Bottom />
    </div>

    <div className={`${GridH}`}>
      <div className={`${GridAnchorLeft} ${GridAnchorTop}`}>
        <div className={`${Sticky}`}>
          <p className={`${StoryBeat}`}>{COPY_TEXT[3]}</p>
        </div>
      </div>
      <div className={`${GridVBody} ${GridH}`}>
        {
          PROCESS.map((item, i) => <div className={`${GridV} ${GridVGap} ${FitContent} ${ScreenHeight} ${GridHCentre}`}>
            <Spine.Top />
            <div style={{ position: "relative" }}>
              <div style={{ width: "100%", aspectRatio: 1 }}>
                <item.icon />
              </div>
            </div>
            <Spine.Bottom />
          </div>)
        }
      </div>
    </div>

    <div className={`${ScreenHeight} ${FitContent}`} style={{ display: "grid" }}>
      <Spine.Top />
      <div className={GridH}>
        <div className={`${GridHCentre}`}>
          <p className={`${TextLg} ${TextCentre} ${StoryBeat}`}><br />{COPY_TEXT[4]}<br /><br /></p>
          <div className={sprinkles({ height: "md2" })} style={{
            position: "relative",
            display: "flex"
          }}>
            <svg
              viewBox="0 0 128 128"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              preserveAspectRatio="none"
              style={{
                position: "absolute",
                top: 0,
                width: "100%",
                height: "100%",
                opacity: 0.5
              }}>
              <path d="M0.5 128V64H127.5V128" stroke="currentColor" />
              <path d="M64 0V128" stroke="currentColor" />
            </svg>
          </div>
        </div>
        <div style={{
          textAlign: "center",
          gridColumn: "8 / span 4"
        }}>
          <p className={TextLg}><br />Email</p>
        </div>
        <div style={{
          textAlign: "center",
          gridColumn: "12 / span 4"
        }}>
          <p className={TextLg}><br />LinkedIn</p>
        </div>
        <div style={{
          textAlign: "center",
          gridColumn: "16 / span 4"
        }}>
          <p className={TextLg}><br />Map</p>
        </div>
      </div>
    </div>
  </div>
}

export default IndexNew